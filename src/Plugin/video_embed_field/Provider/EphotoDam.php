<?php

namespace Drupal\video_embed_field_ephoto\Plugin\video_embed_field\Provider;

use Drupal\video_embed_field\ProviderPluginBase;

/**
 * @VideoEmbedProvider(
 *   id = "ephoto_dam",
 *   title = @Translation("ePhoto DAM")
 * )
 */
class EphotoDam extends ProviderPluginBase {

  /**
   * The URL pattern to check.
   *
   * @var string
   */
  const PATTERN = '/^https?:\/\/(?<host>.*\.ephoto\.fr)\/(?<type>(link)?(publication)?)\/(?<id>.*)$/';

  /**
   * {@inheritdoc}
   */
  public function renderEmbedCode($width, $height, $autoplay) {
    $type = $this->getTypeFromId();
    $render_array = [];
    if ($type === 'link') {
      $render_array = [
        '#type' => 'html_tag',
        '#tag' => 'video',
        '#attributes' => [
          'width' => $width,
          'height' => $height,
          'controls' => '',
          'src' => $this->getVideoId(),
        ],
      ];
    }
    elseif ($type === 'publication') {
      $render_array = [
        '#type' => 'html_tag',
        '#tag' => 'iframe',
        '#attributes' => [
          'width' => $width,
          'height' => $height,
          'frameborder' => '0',
          'allowfullscreen' => 'allowfullscreen',
          'src' => $this->getVideoId(),
        ],
      ];
    }
    return $render_array;
  }

  /**
   * {@inheritdoc}
   */
  public function getRemoteThumbnailUrl() {
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public static function getIdFromInput($input) {
    preg_match(static::PATTERN, $input, $matches);
    $id = !empty($matches['id']) ? $matches['id'] : FALSE;
    $type = !empty($matches['type']) ? $matches['type'] : FALSE;
    $host = !empty($matches['host']) ? $matches['host'] : FALSE;
    if (!empty($id) && !empty($type) && !empty($host) && !empty($matches[0])) {
      return $matches[0];
    }
    return FALSE;
  }

  /**
   * Gets the URL type, link or publication.
   *
   * @return bool|string
   *   The type of video or FALSE if no matching with the pattern.
   */
  protected function getTypeFromId() {
    $id = $this->getVideoId();
    preg_match(static::PATTERN, $id, $matches);
    return !empty($matches['type']) ? $matches['type'] : FALSE;
  }

}
