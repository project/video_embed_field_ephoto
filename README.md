This module add support for embedding ePhoto DAM videos into your website using Video Embed Field

## Installation

To install this module, do the following:

1. Download the Video Embed Field ePhoto module and follow the instruction for
      [installing contributed modules](https://www.drupal.org/docs/8/extending-drupal-8/installing-contributed-modules-find-import-enable-configure-drupal-8).

## Usage

 1. Install module
 2. Add video embed field and enable "ePhoto DAM" provider
 3. Both type links (direct share link or album share link) are supported

Direct share links are embed using a video element. Album share link are embed using an iframe element.

